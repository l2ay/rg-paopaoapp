FROM openjdk:11
WORKDIR /app
RUN apt-get update && apt install ffmpeg -y
COPY ./build/libs/*.jar /converter-0.0.1-SNAPSHOT.jar
COPY ./.env.develop ./app/.env.develop
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/converter-0.0.1-SNAPSHOT.jar"]